/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author João Paulo
 */
public class TrianguloEquilatero extends Retangulo {
    protected double baset;
    
    public TrianguloEquilatero(){}
    
    public TrianguloEquilatero(double base){
        super(base, base*Math.sqrt(3)/2);
        this.baset=base;
    }
    
    @Override
    public double getPerimetro() {
        return 3*baset;
    }
    
    @Override
    public double getArea(){
        return baset*baset*Math.sqrt(3)/4;
    }
}
    
