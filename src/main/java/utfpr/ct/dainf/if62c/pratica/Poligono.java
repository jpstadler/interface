/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utfpr.ct.dainf.if62c.pratica;

/**
 *
 * @author João Paulo
 */
public class Poligono implements FiguraComLados{
    protected double base;
    protected double altura;
    
    public Poligono () {}
    
    public Poligono(double base, double altura){
        this.base = base;
        this.altura = altura;
    }
        
    @Override
    public String getNome(){
        return this.getClass().getSimpleName();
    }

    @Override
    public double getPerimetro() {
        return 2*getLadoMenor()+2*getLadoMaior();
    }

    @Override
    public double getArea() {
        return getLadoMenor()*getLadoMaior();
    }

    @Override
    public double getLadoMenor() {
        if(base<altura)
            return base;
        else
            return altura;
    }

    @Override
    public double getLadoMaior() {
        if(base>altura)
            return base;
        else
            return altura;
    }
}
